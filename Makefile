up: docker-up
down: docker-down
restart: docker-down docker-up
init: docker-down-clear docker-pull docker-build docker-up book-init
test: book-test

docker-up:
	docker-compose up -d

docker-down:
	docker-compose down --remove-orphans

docker-down-clear:
	docker-compose down -v --remove-orphans

docker-pull:
	docker-compose pull

docker-build:
	docker-compose build

book-cli:
	docker-compose run --rm book-php-cli php bin/console

book-init: book-composer-install book-assets-install book-migrations book-ready

book-migrations:
	docker-compose run --rm book-php-cli php bin/console doctrine:migrations:migrate --no-interaction

book-fixtures:
	docker-compose run --rm book-php-cli php bin/console doctrine:fixtures:load --no-interaction

book-composer-install:
	docker-compose run --rm book-php-cli composer install

book-assets-install:
	docker-compose run --rm book-node yarn install
	docker-compose run --rm book-node npm rebuild node-sass
	docker-compose run --rm book-node npm run dev

book-test:
	docker-compose run --rm book-php-cli php bin/console doctrine:fixtures:load --no-interaction
	docker-compose run --rm book-php-cli php bin/phpunit

book-ready:
	docker run --rm -v ${PWD}/book:/app --workdir=/app alpine touch .ready