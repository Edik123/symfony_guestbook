<?php

namespace App\DataFixtures;

use App\Entity\Admin;
use App\Entity\Comment;
use App\Entity\Conference;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;

class AppFixtures extends Fixture
{
    private $encoderFactory;

    public function __construct(EncoderFactoryInterface $encoderFactory)
    {
        $this->encoderFactory = $encoderFactory;
    }

    public function load(ObjectManager $manager)
    {
        $london = new Conference();
        $london->setCity('London');
        $london->setYear('2020');
        $london->setIsInternational(true);
        $manager->persist($london);

        $paris = new Conference();
        $paris->setCity('Paris');
        $paris->setYear('2019');
        $paris->setIsInternational(false);
        $manager->persist($paris);

        $comment = new Comment();
        $comment->setConference($london);
        $comment->setAuthor('Ed');
        $comment->setEmail('ed@example.com');
        $comment->setPhoto('123');
        $comment->setText('This was a great conference.');
        $comment->setState('published');
        $manager->persist($comment);

        $comment2 = new Comment();
        $comment2->setConference($london);
        $comment2->setAuthor('Ed');
        $comment2->setEmail('ed@example.com');
        $comment2->setText('Without status!');
        $manager->persist($comment2);

        $admin = new Admin();
        #$admin->setRoles(['ROLE_ADMIN']);
        $admin->setUsername('admin');
        $admin->setPassword($this->encoderFactory->getEncoder(Admin::class)->encodePassword('admin', null));
        $manager->persist($admin);

        $manager->flush();
    }
}
