<?php

namespace App\Message;

use App\Message\CommentMessage;
use App\Notification\CommentReviewNotification;
use App\Repository\CommentRepository;
use App\Service\ImageOptimizer;
use App\Service\SpamChecker;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bridge\Twig\Mime\NotificationEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Notifier\NotifierInterface;
use Symfony\Component\Workflow\WorkflowInterface;

class CommentMessageHandler implements MessageHandlerInterface
{
    private $em;
    private $bus;
    private $logger;
    private $workflow;
    private $photoDir;
    private $mailer;
    private $notifier;
    private $adminEmail;
    private $spamChecker;
    private $imageOptimizer;
    private $commentRepository;

    public function __construct(
        MessageBusInterface $bus, 
        EntityManagerInterface $em, 
        SpamChecker $spamChecker,
        ImageOptimizer $imageOptimizer, 
        CommentRepository $commentRepository,
        WorkflowInterface $commentStateMachine, 
        LoggerInterface $logger = null,
        MailerInterface $mailer,
        NotifierInterface $notifier, 
        string $adminEmail,
        string $photoDir
    )
    {
        $this->em = $em;
        $this->bus = $bus;
        $this->logger = $logger;
        $this->mailer = $mailer;
        $this->notifier = $notifier;
        $this->photoDir = $photoDir;
        $this->adminEmail = $adminEmail;
        $this->spamChecker = $spamChecker;
        $this->imageOptimizer = $imageOptimizer;
        $this->workflow = $commentStateMachine;
        $this->commentRepository = $commentRepository;
    }

    public function __invoke(CommentMessage $message)
    {
        $comment = $this->commentRepository->find($message->getId());
        if (!$comment) return;

        if ($this->workflow->can($comment, 'accept')) {
            $score = $this->spamChecker->getSpamScore($comment, $message->getContext());
            $transition = 'accept';
            if ($score === 2) {
                $transition = 'reject_spam';
            } elseif ($score === 1) {
                $transition = 'might_be_spam';
            }
            $this->workflow->apply($comment, $transition);
            $this->em->flush();

            $this->bus->dispatch($message);
        } elseif ($this->workflow->can($comment, 'publish') || $this->workflow->can($comment, 'publish_ham')) {
            /*$this->mailer->send((new NotificationEmail())
                ->subject('New comment message')
                ->htmlTemplate('emails/comment_notification.html.twig')
                ->from($this->adminEmail)
                ->to($this->adminEmail)
                ->context(['comment' => $comment])
            );*/
            $notification = new CommentReviewNotification($comment, $message->getReviewUrl());
            $this->notifier->send($notification, ...$this->notifier->getAdminRecipients());
        } elseif ($this->workflow->can($comment, 'optimize')) {
            if ($comment->getPhoto()) {
                $this->imageOptimizer->resize($this->photoDir.'/'.$comment->getPhoto());
            }
            $this->workflow->apply($comment, 'optimize');
            $this->em->flush();
        } elseif ($this->logger) {
            $this->logger->debug('Dropping comment message', ['comment' => $comment->getId(), 'state' => $comment->getState()]);
        }
    }
}
